"""Centralizes functions operations related to update security group"""

import logging
import boto3

EC2_CLIENT = boto3.client("ec2")

LOGGER = logging.getLogger()


def remove_inbound_rule(security_group, ip_addr, ip_protocol, port):
    """Remove an inbound rule in the specified security group"""

    ip_addr_range = ip_addr + "/32"

    EC2_CLIENT.revoke_security_group_ingress(
        GroupId=security_group,
        IpPermissions=[
            {
                "IpProtocol": ip_protocol,
                "FromPort": port,
                "ToPort": port,
                "IpRanges": [{"CidrIp": ip_addr_range}],
            }
        ],
    )
