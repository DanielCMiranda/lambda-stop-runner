"""Fargate module centralizes functions related to AWS Fargate operations"""

import logging
import boto3

ECS_CLIENT = boto3.client("ecs")
EC2_CLIENT = boto3.client("ec2")

LOGGER = logging.getLogger()


def search_tasks(cluster_name, started_by):
    """Get tasks filtered by the "started by" field"""

    response = ECS_CLIENT.list_tasks(
        cluster=cluster_name, startedBy=started_by
    )

    tasks_arn = response.get("taskArns", [])

    return tasks_arn


def stop_task(cluster_name, task_arn):
    """Logic for stopping a specified task"""
    ECS_CLIENT.stop_task(cluster=cluster_name, task=task_arn)


def fetch_task_container_ip(cluster_name, task_arn):
    """
    Fetch the container's ip of a specified task

    This function prioritize to fetch the external ip but if this does not
    exist, will return the private ip.
    """

    private_ip = None
    public_ip = None

    response = ECS_CLIENT.describe_tasks(
        cluster=cluster_name, tasks=[task_arn]
    )

    eni = None
    for detail in response["tasks"][0]["attachments"][0]["details"]:
        if detail["name"] == "networkInterfaceId":
            eni = detail["value"]
        elif detail["name"] == "privateIPv4Address":
            private_ip = detail["value"]

    LOGGER.debug("ENI: %s, private ip: %s", eni, private_ip)

    response = EC2_CLIENT.describe_network_interfaces(
        NetworkInterfaceIds=[eni]
    )

    public_ip = (
        response["NetworkInterfaces"][0].get("Association").get("PublicIp")
    )

    LOGGER.debug("Public ip: %s", public_ip)

    return public_ip if public_ip else private_ip
