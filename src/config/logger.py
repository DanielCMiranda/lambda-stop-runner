"""Centralize logger configurations"""

import logging

LOGGER = logging.getLogger()


def configure_logger():
    """Configure the logger"""

    if LOGGER.handlers:
        for handler in LOGGER.handlers:
            LOGGER.removeHandler(handler)

    log_format = "[%(funcName)20s - %(asctime)s] %(message)s"
    logging.basicConfig(format=log_format, level=logging.INFO)
