"""Start point to run the function locally"""

from handler import lambda_handler


if __name__ == "__main__":
    lambda_handler({}, None)
