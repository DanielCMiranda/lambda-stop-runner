"""This module centralizes functions related to GitLab API operations"""

import json
import logging
import os
import urllib3

LOGGER = logging.getLogger()

HTTP = urllib3.PoolManager()

GITLAB_URL = os.getenv("GITLAB_URL", "https://gitlab.com")


def exist_pending_or_running_jobs(project_id, private_token):
    """
    Check if there are pending or running jobs in the project, not considering
    those using shared runners
    """

    api_base_url = f"{GITLAB_URL}/api/v4/projects/{project_id}/jobs"
    api_url_parameters = "?scope[]=pending&scope[]=running"

    response = HTTP.request(
        "GET",
        api_base_url + api_url_parameters,
        headers={"PRIVATE-TOKEN": private_token},
    )
    if response.status != 200:
        raise Exception("GitLab REST API did not return expected status code")

    jobs = json.loads(response.data.decode("utf-8"))

    count = 0
    for job in jobs:
        runner_info = job.get("runner", {"is_shared": True})
        if not runner_info["is_shared"]:
            count = count + 1

    return count > 0
