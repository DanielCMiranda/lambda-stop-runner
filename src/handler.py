"""Lambda function to stop the Runner manager"""

import logging
import os

from config.logger import configure_logger
from aws.fargate import search_tasks, stop_task, fetch_task_container_ip
from aws.parameter_store import load_from_parameter_store
from aws.security_group import remove_inbound_rule
from gitlab.jobs_rest_api import exist_pending_or_running_jobs

SSM_PARAMETER_NAME = os.getenv("SSM_PARAMETER_NAME", "lambda-gitlab-runner")

LOGGER = logging.getLogger()


def lambda_handler(_event, _context):
    """Entry point for the Lambda function"""

    configure_logger()

    func_config = load_from_parameter_store(SSM_PARAMETER_NAME)

    LOGGER.info("Starting processing the request")

    _process_request(
        func_config["clusterName"],
        func_config["securityGroup"],
        func_config["gitlabApiPrivateToken"],
        func_config["gitlabProjectId"],
    )

    LOGGER.info("Finished processing the request")


def _process_request(
    cluster_name, security_group, gitlab_token, gitlab_project
):
    """Process the Lambda function request"""

    runner_arn_list = _search_for_runner_manager_tasks(cluster_name)

    if len(runner_arn_list) > 0:

        exist_job = _exist_ci_jobs_being_processed(
            gitlab_project, gitlab_token
        )

        if not exist_job:
            _remove_ssh_inbound_rules(
                cluster_name, runner_arn_list, security_group
            )
            _stop_runner_managers(cluster_name, runner_arn_list)


def _search_for_runner_manager_tasks(cluster_name):
    """
    Search for Runner manager tasks

    Those tasks were created by another Lambda function and have the field
    "started_by" filled with a known value.
    """

    LOGGER.info("Will look for Runner manager tasks")

    started_by = "lambda-webhook-function"
    arn_list = search_tasks(cluster_name, started_by)

    LOGGER.info("Found %d Runner manager task(s)", len(arn_list))

    return arn_list


def _exist_ci_jobs_being_processed(gitlab_project, gitlab_token):
    """
    Check if there are jobs in the specified GitLab project being processed
    """

    exist_job = exist_pending_or_running_jobs(gitlab_project, gitlab_token)

    if exist_job:
        LOGGER.info("Found CI jobs being processed")
    else:
        LOGGER.info("No CI job found")

    return exist_job


def _remove_ssh_inbound_rules(cluster_name, runner_arn_list, security_group):
    """
    Removes an existing SSH inbound rule that allows SSH connections from the
    Runner manager
    """

    LOGGER.info(
        "Will remove SSH inbound rules that allow access from the Runner"
    )

    for task_arn in runner_arn_list:
        ip_address = fetch_task_container_ip(cluster_name, task_arn)
        remove_inbound_rule(security_group, ip_address, "tcp", 22)

    LOGGER.info("Inbound rules removed")


def _stop_runner_managers(cluster_name, runner_arn_list):
    """Stop all runner manager tasks created by the other Lambda function"""

    LOGGER.info("Will stop the Runner managers")

    for runner_arn in runner_arn_list:
        stop_task(cluster_name, runner_arn)

    LOGGER.info("All runner managers stopped")
