# lambda-stop-runner

This repository implements a lambda function that is triggered on a regular
schedule using CloudWatch events rule. The function is responsible for stopping
the GitLab Runner manager Fargate Task.

Note: This lambda function is used in conjunction with [another one](https://gitlab.com/DanielCMiranda/lambda-webhook-start-runner)
that is responsible to start the Runner Fargate Task when there are CI jobs to
be processed and no Runner is running.

## Required tools

If you want to deploy the function using the provided [SAM template](https://gitlab.com/DanielCMiranda/lambda-stop-runner/-/blob/master/template.yml),
you will need to install the [AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

## Lambda Deployment

### Deploying using SAM

``` bash
sam package \
  --template-file template.yml \
  --output-template-file package.yml \
  --s3-bucket <your-s3-bucket>
```

``` bash
sam deploy \
  --template-file package.yml \
  --stack-name <your-stack-name> \
  --capabilities CAPABILITY_IAM
```

Note: for convenience, you can use the following command for deploying the
function:

`make deploy s3_bucket=<bucket-name> stack_name=<stack_name>`

### AWS permissions for deploying using SAM

The AWS user deploying the function must have the following IAM permissions
in order to deploy successfully:

``` json
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Sid":"ListDeleteAndCreateRoles",
         "Effect":"Allow",
         "Action":[
            "iam:AttachRolePolicy",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:DetachRolePolicy",
            "iam:GetRole",
            "iam:PassRole",
            "iam:PutRolePolicy",
            "iam:DeleteRolePolicy"
         ],
         "Resource":[
            "arn:aws:iam::*:role/{SAM_STACK_NAME}-*"
         ]
      },
      {
         "Sid":"CloudFormationCreateChangeSet",
         "Effect":"Allow",
         "Action":[
            "cloudformation:CreateChangeSet"
         ],
         "Resource":[
            "arn:aws:cloudformation:us-east-1:aws:transform/Serverless-2016-10-31",
            "arn:aws:cloudformation:*:*:stack/{SAM_STACK_NAME}/*"
         ]
      },
      {
         "Sid":"S3Bucket",
         "Effect":"Allow",
         "Action":[
            "s3:GetObject"
         ],
         "Resource":[
            "arn:aws:s3:::{SAM_S3_BUCKET}",
            "arn:aws:s3:::{SAM_S3_BUCKET}/*"
         ]
      },
      {
         "Sid":"SSMParameters",
         "Effect":"Allow",
         "Action":[
            "ssm:GetParameter"
         ],
         "Resource":[
            "arn:aws:ssm:*:*:parameter/lambda-gitlab-runner"
         ]
      },
      {
         "Sid":"LambdaOperations",
         "Effect":"Allow",
         "Action":[
            "lambda:CreateFunction",
            "lambda:GetFunction",
            "lambda:GetFunctionConfiguration",
            "lambda:DeleteFunction",
            "lambda:AddPermission",
            "lambda:RemovePermission",
            "lambda:UpdateFunctionCode"
         ],
         "Resource":[
            "arn:aws:lambda:*:*:function:stopGitlabRunnerFunction"
         ]
      },
      {
         "Sid":"CloudWatchEvents",
         "Effect":"Allow",
         "Action":[
            "events:PutRule",
            "events:DeleteRule",
            "events:DescribeRule",
            "events:PutTargets",
            "events:RemoveTargets"
         ],
         "Resource":[
            "arn:aws:events:*:*:rule/{SAM_STACK_NAME}-*"
         ]
      }
   ]
}
```

Note that `{SAM_STACK_NAME}` and `{SAM_S3_BUCKET}` should be replaced by the
correct values.

## Requirements

### Function parameters stored in SSM

The function relies on the existence of an [AWS SSM parameter](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html)
having the name `lambda-gitlab-runner` and having as value a `JSON` that
includes the following information:

| Key | Description |
|-|-|
| clusterName | Name of the Fargate Cluster where the GitLab Runner Task should be stopped. |
| securityGroup | AWS security group used by the GitLab Runner Task. |
| gitlabApiPrivateToken | GitLab [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) necessary to use the [GitLab API](https://docs.gitlab.com/ee/api/README.html) |
| gitlabProjectId | GitLab project id for the project where CI Jobs should be fetched |

## Optional environment variables

The function allows some customization via environment variables. All of them
are optional.

| Environment variable name | Description | Default value |
|-|-|-|
| SSM_PARAMETER_NAME | Name of the SSM parameter containing the function configurations | lambda-gitlab-runner |
| GITLAB_URL | Base url to be used to invoke the GitLab API | https://gitlab.com |

## Overview of the function

The Lambda function executes the following steps:

* Search if exist a Runner manager Fargate Task running

* If there is a Runner manager Fargate Task running, the function will then
check if there are still CI jobs to be processed in the GitLab project.
This is done using the GitLab API.

* If no CI job is pending, it will remove the inbound rule used to allow SSH
connections from the Runner manager.

* Finally, the function will stop the Runner manager Fargate Task
