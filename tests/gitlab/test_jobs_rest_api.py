"""Tests for the GitLab Jobs API module"""

# pylint: disable=too-few-public-methods

from unittest.mock import patch
import pytest
from gitlab.jobs_rest_api import exist_pending_or_running_jobs


class DataMockResponse:
    """Class to mock response of urllib3 request"""

    def __init__(self, json_data):
        """Constructor"""
        self.data = json_data

    def decode(self, _format):
        """Decode method"""
        return self.data


class RequestMockResponse:
    """Class to mock response of urllib3 request"""

    def __init__(self, json_data, status_code):
        """Constructor"""
        self.data = DataMockResponse(json_data)
        self.status = status_code


@patch("gitlab.jobs_rest_api.HTTP.request")
@pytest.mark.parametrize(
    "api_response, exist_job",
    [
        (RequestMockResponse("[]", 200), False),
        (RequestMockResponse('[{"runner": {"is_shared": false}}]', 200), True),
        (RequestMockResponse('[{"runner": {"is_shared": true}}]', 200), False),
    ],
)
def test_exist_pending_or_running_jobs(
    mocked_http_request, api_response, exist_job
):
    """Test"""

    mocked_http_request.return_value = api_response

    response = exist_pending_or_running_jobs("project-id", "private-token")

    mocked_http_request.assert_called_once()
    assert response == exist_job


@patch(
    "gitlab.jobs_rest_api.HTTP.request",
    return_value=RequestMockResponse("[]", 500),
)
def test_exist_pending_or_running_jobs_when_returned_unexpected_status_code(
    mocked_http_request
):
    """Test"""

    with pytest.raises(Exception) as ex:
        exist_pending_or_running_jobs("project-id", "private-token")

    mocked_http_request.assert_called_once()
    assert "did not return expected status code" in str(ex.value)
