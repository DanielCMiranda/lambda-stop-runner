"""Tests for the fargate module"""

from unittest.mock import patch
import pytest
from aws.fargate import fetch_task_container_ip, search_tasks, stop_task

MOCKED_ECS_DESCRIBE_TASKS_API_RESULT = {
    "tasks": [
        {
            "attachments": [
                {
                    "details": [
                        {"name": "networkInterfaceId", "value": "task-eni"},
                        {"name": "privateIPv4Address", "value": "192.168.0.0"},
                    ]
                }
            ]
        }
    ]
}


@patch("aws.fargate.ECS_CLIENT.list_tasks")
@pytest.mark.parametrize(
    "api_result, expected_result",
    [
        ({"taskArns": ["arn1", "arn2"]}, ["arn1", "arn2"]),
        ({"taskArns": ["arn1"]}, ["arn1"]),
        ({"taskArns": []}, []),
    ],
)
def test_search_tasks(mocked_ecs_list_tasks, api_result, expected_result):
    """Test"""

    mocked_ecs_list_tasks.return_value = api_result

    tasks_arn = search_tasks("my-cluster", "started-by")

    mocked_ecs_list_tasks.assert_called_once()
    assert tasks_arn == expected_result


@patch(
    "aws.fargate.ECS_CLIENT.stop_task",
    return_value={"tasks": [{"containers": [{"taskArn": "my-task-arn"}]}]},
)
def test_start_task(mocked_ecs_stop_task):
    """Test"""
    stop_task("my-cluster", "task-arn")
    mocked_ecs_stop_task.assert_called_once()


@patch(
    "aws.fargate.ECS_CLIENT.describe_tasks",
    return_value=MOCKED_ECS_DESCRIBE_TASKS_API_RESULT,
)
@patch(
    "aws.fargate.EC2_CLIENT.describe_network_interfaces",
    return_value={
        "NetworkInterfaces": [{"Association": {"PublicIp": "10.0.0.1"}}]
    },
)
def test_fetch_task_container_ip_when_exist_public_ip(
    mocked_ecs_desc_tasks, mocked_ec2_desc_net
):
    """Test"""

    ip_add = fetch_task_container_ip("custer-name", "task-arn")

    mocked_ecs_desc_tasks.assert_called_once()
    mocked_ec2_desc_net.assert_called_once()
    assert ip_add == "10.0.0.1"


@patch(
    "aws.fargate.ECS_CLIENT.describe_tasks",
    return_value=MOCKED_ECS_DESCRIBE_TASKS_API_RESULT,
)
@patch(
    "aws.fargate.EC2_CLIENT.describe_network_interfaces",
    return_value={"NetworkInterfaces": [{"Association": {"PublicIp": ""}}]},
)
def test_fetch_task_container_ip_when_no_public_ip_exist(
    mocked_ecs_desc_tasks, mocked_ec2_desc_net
):
    """Test"""

    ip_add = fetch_task_container_ip("custer-name", "task-arn")

    mocked_ecs_desc_tasks.assert_called_once()
    mocked_ec2_desc_net.assert_called_once()
    assert ip_add == "192.168.0.0"
