"""Tests for the handler"""

from unittest.mock import patch

with patch("boto3.client"):
    from handler import lambda_handler

FUNC_CONFIG = {
    "clusterName": "my-cluster",
    "securityGroup": "sg-XYZ",
    "gitlabApiPrivateToken": "my-api-token",
    "gitlabProjectId": "my-api-project-id",
}


# pylint: disable=too-many-arguments
@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@patch("handler.search_tasks", return_value=["task-arn"])
@patch("handler.exist_pending_or_running_jobs", return_value=False)
@patch("handler.fetch_task_container_ip", return_value="10.0.0.1")
@patch("handler.remove_inbound_rule")
@patch("handler.stop_task")
def test_lambda_handler_when_stopping_runner_successfully(
    mocked_load_params,
    mocked_search_tasks,
    mocked_exist_job,
    mocked_fetch_ip,
    mocked_remove_rule,
    mocked_stop_task,
):
    """Test"""

    lambda_handler(None, None)

    mocked_load_params.assert_called_once()
    mocked_search_tasks.assert_called_once()
    mocked_exist_job.assert_called_once()
    mocked_fetch_ip.assert_called_once()
    mocked_remove_rule.assert_called_once()
    mocked_stop_task.assert_called_once()


@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@patch("handler.search_tasks", return_value=["task-arn"])
@patch("handler.exist_pending_or_running_jobs", return_value=True)
def test_lambda_handler_when_not_stopped_due_to_jobs_pending(
    mocked_load_params, mocked_search_tasks, mocked_exist_job
):
    """Test"""

    lambda_handler(None, None)

    mocked_load_params.assert_called_once()
    mocked_search_tasks.assert_called_once()
    mocked_exist_job.assert_called_once()


@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@patch("handler.search_tasks", return_value=[])
def test_lambda_handler_when_no_runner_up(
    mocked_load_params, mocked_search_tasks
):
    """Test"""

    lambda_handler(None, None)

    mocked_load_params.assert_called_once()
    mocked_search_tasks.assert_called_once()
